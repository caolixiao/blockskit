//
//  BlocksKit
//
//  The Objective-C block utilities you always wish you had.
//
//  Copyright (c) 2011-2012, 2013-2014 Zachary Waldowski
//  Copyright (c) 2012-2013 Pandamonia LLC
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <Foundation/Foundation.h>

//! Project version number for BlocksKit.
FOUNDATION_EXPORT double BlocksKitVersionNumber;

//! Project version string for BlocksKit.
FOUNDATION_EXPORT const unsigned char BlocksKitVersionString[];

#import <LibBlocksKit/BKDefines.h>
#import <LibBlocksKit/BKMacros.h>
#import <LibBlocksKit/NSArray+BlocksKit.h>
#import <LibBlocksKit/NSDictionary+BlocksKit.h>
#import <LibBlocksKit/NSIndexSet+BlocksKit.h>
#import <LibBlocksKit/NSInvocation+BlocksKit.h>
#import <LibBlocksKit/NSNumber+BlocksKit.h>
#import <LibBlocksKit/NSMapTable+BlocksKit.h>
#import <LibBlocksKit/NSMutableArray+BlocksKit.h>
#import <LibBlocksKit/NSMutableDictionary+BlocksKit.h>
#import <LibBlocksKit/NSMutableIndexSet+BlocksKit.h>
#import <LibBlocksKit/NSMutableOrderedSet+BlocksKit.h>
#import <LibBlocksKit/NSMutableSet+BlocksKit.h>
#import <LibBlocksKit/NSObject+BKAssociatedObjects.h>
#import <LibBlocksKit/NSObject+BKBlockExecution.h>
#import <LibBlocksKit/NSObject+BKBlockObservation.h>
#import <LibBlocksKit/NSOrderedSet+BlocksKit.h>
#import <LibBlocksKit/NSSet+BlocksKit.h>
#import <LibBlocksKit/NSTimer+BlocksKit.h>

